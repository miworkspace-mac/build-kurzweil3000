#!/usr/bin/env ruby

require 'rubygems'
require 'nokogiri'
require 'open-uri'

begin
doc = Nokogiri::HTML(open("https://kurzweiledu.com/news-resources/software-updates/ki-3000-mac-version5-standalone-updates.html", "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36"))
rescue
puts "Failed to fetch download page"
exit 1
end

link = doc.xpath('//a').find {|x| x.inner_html.match(/K3000/i)}

if link

puts "#{link['href']}"

end