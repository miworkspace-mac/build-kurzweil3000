#!/bin/bash -ex

# CONFIG
prefix="Kurzweil3000"
suffix=""
munki_package_name="Kurzweil3000"
display_name="Kurzweil 3000"
icon_name=""
url=`./finder.rb`

# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36' "${url}"

## EXAMPLE: unpacking a flat package to find appropriate things
## Mount disk image on temp space
# mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack
# /usr/sbin/pkgutil --expand "${mountpoint}/*.pkg" pkg
# mkdir build-root
# (cd build-root; pax -rz -f ../pkg/*/Payload)
# hdiutil detach "${mountpoint}"

## Find all the appropriate apps, etc, and then turn that into -f's
# key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
# echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.dmg ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
# perl -p -i -e 's/build-root//' app.plist
## END EXAMPLE

# Build pkginfo
/usr/local/munki/makepkginfo app.dmg > app.plist

plist=`pwd`/app.plist

# Obtain version info CFBundleVersion
version=`/usr/libexec/PlistBuddy -c "print :installs:0:CFBundleVersion" app.plist`

# For silent packages, configure blocking_applications and unattended_install
# defaults write "${plist}" blocking_applications -array "VirtualBox.app" "/Path/To/Executable" "iTunesHelper"
# defaults write "${plist}" unattended_install -bool YES

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.dmg"
defaults write "${plist}" minimum_os_version "10.8.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" version "${version}"

# Obtain update description from MacUpdate and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.dmg   ${prefix}-${version}${suffix}.dmg
mv app.plist ${prefix}-${version}${suffix}.plist
